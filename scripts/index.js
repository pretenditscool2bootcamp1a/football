"use strict";


window.onload = function () {

    initStatesDropdown();
    document.getElementById("footballteams").onchange = clearpara;
    document.getElementById("myform").onsubmit = setpara;
}

function clearpara(){
    const ptrpara = document.getElementById("1stpara");
    ptrpara.innerHTML = "" ;
}
function initStatesDropdown() {
    let teams = [
        { code: "DAL", name: "Dallas Cowboys", plays: "Arlington, TX" },
        { code: "DEN", name: "Denver Broncos", plays: "Denver, CO" },
        { code: "HOU", name: "Houston Texans", plays: "Houston, TX" },
        {
            code: "KAN", name: "Kansas City Chiefs",
            plays: "Kansas City, MO"
        },
    ];

    const teamslist = document.getElementById("footballteams");
    let theOption = new Option("Select a team", "");
    teamslist.appendChild(theOption);
    for (let i = 0; i < teams.length; i++) {
        // create the option element and set the text and
        // value at the same time
        let theOption = new Option(teams[i].name, teams[i].code);
        theOption.plays = teams[i].plays;
        // append the option as a child of (inside) the
        // select element
        teamslist.appendChild(theOption);
    }
}

function setpara() {
    const ptrpara = document.getElementById("1stpara");
    const ptrfootballteams = document.getElementById("footballteams");
    let selectedidx = ptrfootballteams.selectedIndex;
  //  alert(selectedidx);
    if (selectedidx == 0) {
        ptrfootballteams.innerHTML = "";
    }
    ptrpara.innerHTML = "You selected the " + ptrfootballteams.options[selectedidx].text + " who play in " + ptrfootballteams.options[selectedidx].plays;
    return false;
}